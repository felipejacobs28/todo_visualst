﻿using System.ComponentModel.DataAnnotations;

namespace TodoListWeb.Model
{
    public class Task
    {
        //USER: u-visualst PASS: vba_test123
        [Key]
        public int Id { get; set; }
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}