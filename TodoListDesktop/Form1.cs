﻿using System;
using System.Collections.Generic;
using TodoListDesktop.Views;
using System.Windows.Forms;
using TodoListDesktop.Services;
using TodoListDesktop.Model;
using TodoListDesktop.Repository;
namespace TodoListDesktop
{
    public partial class Form1 : Form
    {
        TaskStore taskStore = TaskStore.GetInstance;

        public Form1()
        {
            InitializeComponent();
        }


        private void btnNewTask_Click(object sender, EventArgs e)
        {
            DbManagement dbm = new DbManagement();
            string[] field = { "id" };
            List<object> lastIndex = dbm.ListGenericData("dbo.Task", field, "ORDER BY ID DESC");
            TasksItems task = new TasksItems(_status: false, _message: txtTask.Text);
            if (taskStore.tasks != null)
            {
                taskStore.tasks.Add(task);
            }
            else
            {
                taskStore.tasks = new List<TasksItems> { task };
            }

            string[] tableColumns = { "id", "message", "status" };
            object[] rowValues = { lastIndex[0], txtTask.Text, false };

            dbm.InsertDataToRow("dbo.Task", tableColumns, rowValues);

            int done = TaskStore.getTaskStatus(true).Count;
            int todo = TaskStore.getTaskStatus(false).Count;
            lbl_todo.Text = "Por Hacer: " + todo.ToString();
            lbl_done.Text = "Resueltas: " + done.ToString();
            listBox1.Items.Add(task.message);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            bool countActives = listBox1.SelectedItems.Count > TaskStore.getTaskStatus(true).Count;
            if (countActives)
            {
                foreach (var taskLists in listBox1.SelectedItems)
                {
                    TasksItems tmp = taskStore.tasks.Find(x => x.message == (string)taskLists);
                    if (countActives)
                    {
                        if (!tmp.status)
                        {
                            taskStore.tasks.Find(x => x.message == (string)taskLists).status = true;
                        }
                    }

                }
            }
            else
            {
                taskStore.resetStatus();
                foreach (var taskLists in listBox1.SelectedItems)
                {
                    taskStore.tasks.Find(x => x.message == (string)taskLists).status = true;
                }
            }

            int done = TaskStore.getTaskStatus(true).Count;
            int todo = TaskStore.getTaskStatus(false).Count;
            lbl_todo.Text = "Por Hacer: " + todo.ToString();
            lbl_done.Text = "Resueltas: " + done.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DbManagement dbm = new DbManagement();
            string[] tableColumns = { "id", "message", "status" };
            List<TasksItems> tasksList = dbm.ListTaskData("dbo.Task", tableColumns, null);
            foreach (TasksItems tasksItems in tasksList)
            {
                listBox1.Items.Add(tasksItems.message);
                if (taskStore.tasks != null)
                {
                    taskStore.tasks.Add(tasksItems);
                }
                else
                {
                    taskStore.tasks = new List<TasksItems> { tasksItems };
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count > 1)
            {
                MessageBox.Show("Para esta accion usa el borrado masivo");
            }
            else
            {
                //Corregir borrado con indice diferente
                TasksItems tmp = taskStore.tasks.Find(x => x.message == listBox1.SelectedItems[0].ToString());
                DbManagement dbManagement = new DbManagement();
                bool isDeleted = dbManagement.DeleteDataFromRow("dbo.Task", "id", tmp.id);
                if (isDeleted)
                {
                    listBox1.Items.RemoveAt(int.Parse(tmp.id));
                }
            }
        }

        private void btnMassive_Click(object sender, EventArgs e)
        {
            DbManagement dbManagement = new DbManagement();
            foreach (var selectItem in listBox1.SelectedItems)
            {
                TasksItems tmp = taskStore.tasks.Find(x => x.message == (string)selectItem);
                bool isDeleted = dbManagement.DeleteDataFromRow("dbo.Task", "id", tmp.id);
                //Tarea: Terminar desarrollo del masivo
                if (isDeleted)
                {
                    listBox1.Items.RemoveAt(int.Parse(tmp.id));
                }
            }
        }
    }

}
