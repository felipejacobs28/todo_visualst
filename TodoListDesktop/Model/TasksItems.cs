﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoListDesktop.Model
{
    public class TasksItems
    {
        public TasksItems(string _id, string _message, bool _status)
        {
            id = _id;
            message = _message;
            status = _status;
        }
        public TasksItems(string _message, bool _status)
        {
            message = _message;
            status = _status;
        }
        public string id { get; set; }
        public string message { get; set; }
        public bool status { get; set; }

    }
}
