﻿using System.Collections.Generic;
using TodoListDesktop.Model;

namespace TodoListDesktop.Services
{
    public class TaskStore
    {
        private List<TasksItems> _tasks;
        public List<TasksItems> tasks { get; set; }

        private static TaskStore _session;
        
        private TaskStore() { }
        public static TaskStore GetInstance
        {
            get
            {
                if (_session == null) _session = new TaskStore();
                return _session;
            }
        }
        public void resetStatus()
        {
            foreach (TasksItems task in _session.tasks)
            {
                task.status = false;
            }
        }
        public static List<TasksItems> getTaskStatus(bool status) {
            List<TasksItems> resultTasks = new List<TasksItems>();
            if (_session.tasks.Count > 0)
            {
                foreach (TasksItems task in _session.tasks)
                {
                    if (status == task.status)
                    {
                        resultTasks.Add(task);
                    }
                }
            }
            return resultTasks;
        }
    }
}
