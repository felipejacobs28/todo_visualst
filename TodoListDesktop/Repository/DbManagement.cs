﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using TodoListDesktop.Model;
namespace TodoListDesktop.Repository
{
    class DbManagement
    {
        private SqlConnection sqlConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["todoSql"].ConnectionString;
            return new SqlConnection(conString);
        }
        public List<Object> ListGenericData(string rowName, string[] customData, string extra)
        {
            List<Object> resultList = new List<Object>();
            try
            {
                using(SqlConnection connection = sqlConnection())
                {
                    string query = $"select {formatSelectStructure(customData, null)} from {rowName} {extra}";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.CommandType = CommandType.Text;
                    connection.Open();

                    using(SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            foreach (string columnNames in customData)
                            {
                                resultList.Add(dr[columnNames]);                         
                            }
                        }
                    }
                }
                return resultList;
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
                return resultList;
            }
        }
        public List<TasksItems> ListTaskData(string rowName, string[] customData, string extra)
        {
            List<TasksItems> resultList = new List<TasksItems>();
            try
            {
                using (SqlConnection connection = sqlConnection())
                {
                    string query = $"select {formatSelectStructure(customData, null)} from {rowName} {extra}";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.CommandType = CommandType.Text;
                    connection.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            resultList.Add(new TasksItems(dr["id"].ToString(), dr["message"].ToString(), Boolean.Parse(dr["status"].ToString())));
                        }
                    }
                }
                return resultList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return resultList;
            }
        }
        public void InsertDataToRow(string tableName, string[] rowTableValues, object[] rowValues)
        {
            string cmdText = $"insert into {tableName} {formatStringSqlData(rowTableValues, null)} values {formatStringSqlData(rowTableValues, "@")}";
            try
            {
                using (SqlConnection conector = sqlConnection())
                using (SqlCommand cmd = new SqlCommand(cmdText, conector))
                {
                    conector.Open();
                    {
                        foreach (string rowVals in rowTableValues)
                        {
                            var index = Array.IndexOf(rowTableValues, rowVals);
                            cmd.Parameters.AddWithValue($"@{rowVals}", rowValues[index]);
                        }
                        cmd.ExecuteNonQuery();
                        conector.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        public bool DeleteDataFromRow(string tableName, string columnId, object columnValue)
        {
            bool result = false;
            string cmdText = $"delete from {tableName} where {columnId} = {columnValue}";
            try
            {
                using (SqlConnection conector = sqlConnection())
                using (SqlCommand cmd = new SqlCommand(cmdText, conector))
                {
                    conector.Open();
                    cmd.Parameters.AddWithValue($"@{columnId}", columnValue);
                    cmd.ExecuteNonQuery();
                    conector.Close();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }

        private string formatSelectStructure(string[] args, string extras)
        {
            string composedRowVals = "";
            foreach (string rowVals in args)
            {
                if (extras != null)
                {
                    composedRowVals += extras + rowVals + ",";
                }
                else
                {
                    composedRowVals += rowVals + ",";
                }
            }
            composedRowVals = composedRowVals.Substring(0, composedRowVals.Length - 1);
            return composedRowVals;
        }
        private string formatStringSqlData(string[] args, string extras)
        {
            string composedRowVals = "(";
            foreach (string rowVals in args)
            {
                if (extras != null)
                {
                    composedRowVals += extras + rowVals + ",";
                }
                else
                {
                    composedRowVals += rowVals + ",";
                }
            }
            composedRowVals = composedRowVals.Substring(0, composedRowVals.Length - 1);
            composedRowVals += ")";
            return composedRowVals;
        }
    }
}
