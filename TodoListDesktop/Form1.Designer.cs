﻿namespace TodoListDesktop
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTask = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnNewTask = new System.Windows.Forms.Button();
            this.lbl_todo = new System.Windows.Forms.Label();
            this.lbl_done = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnMassive = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(462, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lista de tareas";
            // 
            // txtTask
            // 
            this.txtTask.Location = new System.Drawing.Point(264, 131);
            this.txtTask.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTask.Name = "txtTask";
            this.txtTask.Size = new System.Drawing.Size(781, 26);
            this.txtTask.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(62, 132);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nueva Tarea:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(66, 214);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox1.Size = new System.Drawing.Size(1142, 424);
            this.listBox1.TabIndex = 3;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // btnNewTask
            // 
            this.btnNewTask.Location = new System.Drawing.Point(1078, 131);
            this.btnNewTask.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnNewTask.Name = "btnNewTask";
            this.btnNewTask.Size = new System.Drawing.Size(132, 35);
            this.btnNewTask.TabIndex = 4;
            this.btnNewTask.Text = "Nueva Tarea";
            this.btnNewTask.UseVisualStyleBackColor = true;
            this.btnNewTask.Click += new System.EventHandler(this.btnNewTask_Click);
            // 
            // lbl_todo
            // 
            this.lbl_todo.AutoSize = true;
            this.lbl_todo.Location = new System.Drawing.Point(696, 666);
            this.lbl_todo.Name = "lbl_todo";
            this.lbl_todo.Size = new System.Drawing.Size(84, 20);
            this.lbl_todo.TabIndex = 5;
            this.lbl_todo.Text = "Por Hacer:";
            // 
            // lbl_done
            // 
            this.lbl_done.AutoSize = true;
            this.lbl_done.Location = new System.Drawing.Point(1074, 666);
            this.lbl_done.Name = "lbl_done";
            this.lbl_done.Size = new System.Drawing.Size(85, 20);
            this.lbl_done.TabIndex = 6;
            this.lbl_done.Text = "Resueltas:";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(67, 663);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(118, 51);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Borrar tarea";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnMassive
            // 
            this.btnMassive.Location = new System.Drawing.Point(247, 666);
            this.btnMassive.Name = "btnMassive";
            this.btnMassive.Size = new System.Drawing.Size(144, 48);
            this.btnMassive.TabIndex = 8;
            this.btnMassive.Text = "Borrado masivo";
            this.btnMassive.UseVisualStyleBackColor = true;
            this.btnMassive.Click += new System.EventHandler(this.btnMassive_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1260, 726);
            this.Controls.Add(this.btnMassive);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lbl_done);
            this.Controls.Add(this.lbl_todo);
            this.Controls.Add(this.btnNewTask);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTask);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "TODO-List";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTask;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnNewTask;
        private System.Windows.Forms.Label lbl_todo;
        private System.Windows.Forms.Label lbl_done;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnMassive;
    }
}

