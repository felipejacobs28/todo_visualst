﻿using TodoListDesktop.Services;

using System.Collections.Generic;
using TodoListDesktop.Model;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TodoListDesktopTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void getTaskStatus_void_list()
        {
            TaskStore tsk = TaskStore.GetInstance;
            tsk.tasks = new List<TasksItems>();
            CollectionAssert.AreEqual(TaskStore.getTaskStatus(false), new List<TasksItems>());
        }
        [TestMethod]
        public void getTaskStatus_false_status_list()
        {
            TaskStore tsk = TaskStore.GetInstance;
            tsk.tasks = new List<TasksItems>();
            TasksItems t1 = new TasksItems("test1", false);
            TasksItems t2 = new TasksItems("test2", true);
            tsk.tasks.Add(t1);
            tsk.tasks.Add(t2);
            Assert.AreEqual(TaskStore.getTaskStatus(false).Count, 1);
        }
    }
}